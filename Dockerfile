FROM python

RUN mkdir /app

WORKDIR /app

COPY simple.py .

CMD ["python3", "simple.py"]